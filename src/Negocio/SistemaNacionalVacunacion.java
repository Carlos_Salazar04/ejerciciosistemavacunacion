/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.util.Random;
import ufps.util.colecciones_seed.Cola;
import ufps.util.colecciones_seed.ListaCD;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author madarme
 */
public class SistemaNacionalVacunacion {

    private int etapa;
    private String urlDpto, urlMunicipio, urlPersona, urlContenedor;
    private final ListaCD<Departamento> dptos = new ListaCD();
    private final ListaCD<NotificacionVacunado> registros = new ListaCD();
    private Proveedor[] proveedores;

    public SistemaNacionalVacunacion() {
    }

    public ListaCD<Departamento> getDptos() {
        return dptos;
    }

    public ListaCD<NotificacionVacunado> getRegistros() {
        return registros;
    }

    public int getEtapa() {
        return etapa;
    }

    public void setEtapa(int etapa) {
        this.etapa = etapa;
    }

    public String getUrlDpto() {
        return urlDpto;
    }

    public void setUrlDpto(String urlDpto) {
        this.urlDpto = urlDpto;
    }

    public String getUrlMunicipio() {
        return urlMunicipio;
    }

    public void setUrlMunicipio(String urlMunicipio) {
        this.urlMunicipio = urlMunicipio;
    }

    public String getUrlPersona() {
        return urlPersona;
    }

    public void setUrlPersona(String urlPersona) {
        this.urlPersona = urlPersona;
    }

    public String getUrlContenedor() {
        return urlContenedor;
    }

    public void setUrlContenedor(String urlContenedor) {
        this.urlContenedor = urlContenedor;
    }

    public Proveedor[] getProveedores() {
        return proveedores;
    }

    public void setProveedores(Proveedor[] proveedores) {
        this.proveedores = proveedores;
    }

    // Métodos de mis requerimientos:
    /**
     * Punto 1
     */
    public void cargarPersonas() {
        ArchivoLeerURL file = new ArchivoLeerURL(this.urlPersona);
        Object v[] = file.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            //cedula;nombre;fechanacimiento;id_municipio_inscripcion;email
            String linea = v[i].toString();
            //100200;nombre100200;1958-12-3;5;nombre100200@sindir.co
            String datos[] = linea.split(";");
            //public Persona(long cedula, String nombre, LocalDate fechaNacimiento, String email) {
            Persona nueva = new Persona(Long.parseLong(datos[0]), datos[1], datos[2], datos[4]);
            Municipio m = this.getMunicipio(Integer.parseInt(datos[3]));
            if (m == null) {
                throw new RuntimeException("No se puede cargar personas , el municipio asociado no existe");
            }
            m.registrarPersona(nueva);
        }
    }

    private Municipio getMunicipio(int id_municipio) {
        for (Departamento d : this.getDptos()) {
            Municipio m = d.getMunicipio(id_municipio);
            if (m != null) {
                return m;
            }
        }
        return null;
    }

    /**
     * Punto 2
     */
    public void cargarDptos() {

        ArchivoLeerURL file = new ArchivoLeerURL(this.urlDpto);
        Object v[] = file.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            //id_dpto;Nombre Departamento
            String linea = v[i].toString();
            //1;Antioquia
            String datos[] = linea.split(";");
            //datos[0]=1 y datos[1]=Antioquia
            Departamento nuevo = new Departamento(Integer.parseInt(datos[0]), datos[1]);
            this.dptos.insertarAlFinal(nuevo);
        }

    }

    /**
     * Punto 3
     */
    public void cargarMunicipios() {
        if (this.dptos.esVacia()) {
            throw new RuntimeException("No se puede cargar municipios si no hay información de dptos");
        }
        ArchivoLeerURL file = new ArchivoLeerURL(this.urlMunicipio);
        Object v[] = file.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            //id_dpto;id_municipio;nombreMunicipio
            String linea = v[i].toString();
            //1;3;Abejorral
            String datos[] = linea.split(";");
            //datos[0]=1 , datos[1]=3 y datos[2]=Abejorral
            Municipio nuevo = new Municipio(Integer.parseInt(datos[1]), datos[2]);
            Departamento dpto = this.getDpto(Integer.parseInt(datos[0]));
            if (dpto == null) {
                throw new RuntimeException("No se puede cargar municipios , no existe el dpto asociado");
            }
            dpto.getMunicipios().insertarAlFinal(nuevo);

        }

    }

    private Departamento getDpto(int id_dpto) {
        for (Departamento dato : this.dptos) {
            if (dato.getId_dpto() == id_dpto) {
                return dato;
            }
        }
        return null;
    }

    /**
     * Punto 4.
     */
    public void cargarProveedores() {
        ArchivoLeerURL file = new ArchivoLeerURL(this.urlContenedor);
        Object v[] = file.leerArchivo();
        int vacunasRegistradas = 0;
        //Crear el arreglo de proveedores a partir de la info de la URL
        this.proveedores = new Proveedor[v.length - 1];
        //Contador de vacunas registradas
        int conVacunas = 0;
        for (int i = 1; i < v.length; i++) {
            //id_proveedor;nombre_proveedor;dosis;fecha_expiracion
            String linea = v[i].toString();
            //1;qqta_1;1000;18/04/2021
            String datos[] = linea.split(";");
            //datos[0]=1 , datos[1]=qqta_1, datos[2]=1000 y datos[3]=18/04/2021
            Proveedor nuevo = new Proveedor(Integer.parseInt(datos[0]), datos[1], Integer.parseInt(datos[2]), datos[3], conVacunas);
            conVacunas += Integer.parseInt(datos[2]);
            this.proveedores[i - 1] = nuevo;
        }
    }

    public String getListadoProveedores() {
        String msg = "";
        for (Proveedor p : this.proveedores) {
            msg += p.toString() + "\n";
        }
        return msg;
    }

    /**
     * Punto 5
     *
     * @param fecha_inicio
     */
    public void procesarNotificacion(LocalDate fecha_inicio) {
        Cola<Municipio> municipiosAVacunar = this.getMunicipiosValidos(true);
        LocalDateTime fechaVacunacion = LocalDateTime.of(fecha_inicio, LocalTime.of(6, 0));

        this.ordenarProveedores();
        int proveedoresValidos = this.proveedores.length;
        while (!municipiosAVacunar.esVacia() && proveedoresValidos > 0) {
            while (fechaVacunacion.getDayOfWeek().getValue() > 5) {
                fechaVacunacion.plusDays(1);
            }
            while (this.proveedores[proveedoresValidos - 1].getVacunas().esVacia()) {
                proveedoresValidos--;
            }
            municipiosAVacunar = entregarTandaVacunas(municipiosAVacunar, fechaVacunacion.toLocalDate(), proveedoresValidos);
            fechaVacunacion = this.avanzarMinutos(fechaVacunacion);
        }
    }

    private LocalDateTime avanzarMinutos(LocalDateTime fechaVacunacion) {
        fechaVacunacion = fechaVacunacion.plusMinutes(20);
        if (fechaVacunacion.getHour() == 12) {
            fechaVacunacion = LocalDateTime.of(fechaVacunacion.getYear(), fechaVacunacion.getMonth(), fechaVacunacion.getDayOfMonth(), 14, 0);
        }
        if (fechaVacunacion.getHour() == 18) {
            fechaVacunacion = LocalDateTime.of(fechaVacunacion.getYear(), fechaVacunacion.getMonth(), fechaVacunacion.getDayOfMonth(), 6, 0);
            fechaVacunacion.plusDays(1);
        }
        return fechaVacunacion;
    }

    private void ordenarProveedores() {
        int max = this.proveedores.length - 1;
        int i = 0;
        while (i < max) {
            if (this.proveedores[i].getCantidadVacunas() == 0) {
                Proveedor auxiliar = this.proveedores[max];
                this.proveedores[max] = this.proveedores[i];
                this.proveedores[i] = auxiliar;
                max--;
            } else {
                i++;
            }
        }
    }

    private Cola<Municipio> entregarTandaVacunas(Cola<Municipio> municipiosAEntregar, LocalDate fechaVacunacion, int proveedoresValidos) {
        Cola<Municipio> nuevoOrden = new Cola();
        Cola<Vacuna> tandaVacunas = this.getTandaVacunaAleatoria(municipiosAEntregar.getTamanio(), proveedoresValidos, fechaVacunacion);
        while (!municipiosAEntregar.esVacia() && !tandaVacunas.esVacia()) {
            Municipio myMunicipio = municipiosAEntregar.deColar();
            Persona myPersona = myMunicipio.getPersonas().deColar();
            Vacuna myVacuna = tandaVacunas.deColar();
            NotificacionVacunado myNotificacion = new NotificacionVacunado(fechaVacunacion, myPersona, myVacuna);
            this.registros.insertarAlFinal(myNotificacion);
            if (!myMunicipio.getPersonas().esVacia()) {
                nuevoOrden.enColar(myMunicipio);
            }
        }
        return nuevoOrden;
    }

    private Cola<Vacuna> getTandaVacunaAleatoria(int cantidadVacunas, int cantidadProveedores, LocalDate fechaVacunacion) {
        Cola<Vacuna> vacunas = new Cola();
        Random rnd = new Random();
        while (cantidadVacunas > 0 && cantidadProveedores > 0) {
            int indice = (int) (rnd.nextDouble() * cantidadProveedores);
            Vacuna myVacuna = this.proveedores[indice].getVacunas().desapilar();
            if (myVacuna.getFecha_expiracion().compareTo(fechaVacunacion) >= 0) {
                vacunas.enColar(myVacuna);
                cantidadVacunas--;
            }
            if (this.proveedores[indice].getCantidadVacunas() == 0) {
                Proveedor auxiliar = this.proveedores[cantidadProveedores - 1];
                this.proveedores[cantidadProveedores - 1] = this.proveedores[indice];
                this.proveedores[indice] = auxiliar;
                cantidadProveedores--;
            }
        }
        return vacunas;
    }

    private Cola<Municipio> getMunicipiosValidos(boolean registrados) {
        Cola<Municipio> municipios = new Cola<>();
        for (Departamento d : this.dptos) {
            Cola<Municipio> lista = d.getTotalMunicipios(registrados);
            while (!lista.esVacia()) {
                municipios.enColar(lista.deColar());
            }
        }
        return municipios;
    }

    /**
     * Punto 5b
     *
     * @return una cadena con los nombres de los municipios que no tienen
     * personas a vacunar
     */
    public String getMunicipios_No_Vacunados() {
        Cola<Municipio> municipiosNoVacunados = getMunicipiosValidos(false);
        String msg = "";
        while (!municipiosNoVacunados.esVacia()) {
            Municipio myMunicipio = municipiosNoVacunados.deColar();
            msg += myMunicipio.toString() + "\n";
        }
        return msg;
    }

    /**
     * Punto 5c
     *
     * @return una cadena con los nombres de los dptos que más personas se les
     * envío las notificaciones
     */
    public String getDpto_Mas_Notificaciones() {
        int contadorPersonasPorMunicipio[] = new int[100000];
        ArchivoLeerURL file = new ArchivoLeerURL(this.urlPersona);
        Object[] datosBruto = file.leerArchivo();
        String answer = "";
        int maxActual = -1;

        for (int i = 1; i < datosBruto.length; i++) {
            String linea = datosBruto[i].toString();
            //cedula(0) ;nombre(1) ;fechanacimiento(2) ;id_municipio_inscripcion(3) ;email(4)
            String[] datos = linea.split(";");
            int idMunicipio = Integer.parseInt(datos[3]);
            contadorPersonasPorMunicipio[idMunicipio]++;
        }

        for (Departamento departamento : this.dptos) {
            int contadorPersonas = 0;
            for (Municipio municipio : departamento.getMunicipios()) {
                contadorPersonas += contadorPersonasPorMunicipio[municipio.getId_municipio()];
            }
            if (maxActual < contadorPersonas) {
                answer = departamento.getNombreDpto();
                maxActual = contadorPersonas;
            }
            else if (maxActual == contadorPersonas) {
                answer += " " + departamento.getNombreDpto();
            }
        }
        return answer;
    }
}
