/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import Negocio.SistemaNacionalVacunacion;
import java.time.LocalDate;
import java.time.Month;

/**
 *
 * @author madar
 */
public class TestSistemaVacunacion {

    public static void main(String[] args) {
        SistemaNacionalVacunacion mySistema = new SistemaNacionalVacunacion();
        mySistema.setUrlDpto("https://gitlab.com/Carlos_Salazar04/ejerciciosistemavacunacion/-/raw/master/src/ArchivosLectura/departamentos.csv");
        mySistema.setUrlMunicipio("https://gitlab.com/Carlos_Salazar04/ejerciciosistemavacunacion/-/raw/master/src/ArchivosLectura/municipios.csv");
        mySistema.setUrlPersona("https://gitlab.com/Carlos_Salazar04/ejerciciosistemavacunacion/-/raw/master/src/ArchivosLectura/personas.csv");
        mySistema.setUrlContenedor("https://gitlab.com/madarme/archivos-persistencia/-/raw/master/vacuna/vacuna.csv");
        mySistema.cargarDptos();
        mySistema.cargarMunicipios();
        mySistema.cargarPersonas();
        mySistema.cargarProveedores();
        mySistema.procesarNotificacion(LocalDate.of(1880, Month.MARCH, 17));
        System.out.println("notificaciones:\n" + mySistema.getRegistros());
    }

}
