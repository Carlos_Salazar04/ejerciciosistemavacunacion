/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.time.LocalDate;

/**
 *
 * @author madarme
 */
public class NotificacionVacunado {

    private LocalDate fechaVacunar;
    private Persona persona_vacunar;
    private Vacuna vacuna_asignada;

    public NotificacionVacunado() {
    }

    public NotificacionVacunado(LocalDate fechaVacunar, Persona persona_vacunar, Vacuna vacuna_asignada) {
        this.fechaVacunar = fechaVacunar;
        this.persona_vacunar = persona_vacunar;
        this.vacuna_asignada = vacuna_asignada;
    }

    public LocalDate getFechaVacunar() {
        return fechaVacunar;
    }

    public void setFechaVacunar(LocalDate fechaVacunar) {
        this.fechaVacunar = fechaVacunar;
    }

    public Persona getPersona_vacunar() {
        return persona_vacunar;
    }

    public void setPersona_vacunar(Persona persona_vacunar) {
        this.persona_vacunar = persona_vacunar;
    }

    public Vacuna getVacuna_asignada() {
        return vacuna_asignada;
    }

    public void setVacuna_asignada(Vacuna vacuna_asignada) {
        this.vacuna_asignada = vacuna_asignada;
    }

    @Override
    public String toString() {
        return "\n NotificacionVacunado{" + "fechaVacunar=" + fechaVacunar + ", persona_vacunar=" + persona_vacunar + ", vacuna_asignada=" + vacuna_asignada + '}';
    }
}
